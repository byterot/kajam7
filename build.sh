#!/bin/bash -e

#--------------
#- build game -
#--------------
mkdir -p build
mkdir -p bin
cd build
cmake -DCMAKE_TOOLCHAIN_FILE="toolchains/dos.cmake" .. 

# get number of cores + 1
make -j$[$(grep cores /proc/cpuinfo | head -n 1 | grep -oe [0-9]*) + 1]
cd ..
./upx -9 build/$1 || true 
#./upx --ultra-brute build/$1 || true 
cp build/$1 bin/$1.EXE

#---------------
#- build assets -
#----------------
cd src/tools/
g++ -o packer packer.cpp
cd ../..
src/tools/packer bin/$1.DAT || true

cp sound/* bin/

echo 'total game size:'
du -bh bin/
du -bh bin/$1.EXE
du -bh bin/$1.DAT
#stat --printf="%n -> %s\n" bin/*
