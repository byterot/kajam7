#pragma once

#include <memory>

class Sound {
  int sampleCount;
  std::unique_ptr<int16_t[]> samples;
public:
  Sound(int sampleCount);
  Sound(Sound&& old);
  int getSampleCount() const;
  int16_t* getSamplePointer(int offset) const;
};

