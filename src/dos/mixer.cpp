#include <algorithm>
#include <cstring>
#include "mixer.h"
#include "soundblaster.h"
//#include "log.hpp"
#include <dos.h>

Mixer::Mixer() {
  samplesPerBlock = soundblasterGetSamplesPerBuffer();
  data = std::make_unique<int16_t[]>(2*samplesPerBlock);
  memset(data.get(), 0, samplesPerBlock);
}

int16_t* Mixer::getNextBlock() {
  currentBlock = 1 - currentBlock;
  canmix = true;
  return data.get();// + samplesPerBlock; // * currentBlock;
}


void Mixer::play(std::shared_ptr<Sound> newsound) {
  disable();
  auto s = find_if(sounds.begin(), sounds.end(), [&](auto const& a) { return a.sound == newsound; });
  if(s != sounds.end())
    s->offset = 0;
  else
    sounds.emplace_back(0, newsound);
  enable();
}

static int16_t mix(int32_t a, int32_t b) {
  int32_t res = a + b;
  if(res > INT16_MAX)
    res = INT16_MAX;
  if(res < INT16_MIN)
    res = INT16_MIN;
  return res;
}

void Mixer::mix() {
  if(!canmix)
    return;

  int16_t* buf = data.get();
  memset(buf, 0, samplesPerBlock*2);

  for(auto& snd: sounds) {
    int len = snd.sound->getSampleCount() - snd.offset;
    int16_t* soundBuf = snd.sound->getSamplePointer(snd.offset);
    if(len > samplesPerBlock)
      len = samplesPerBlock;

    for(int offset = 0; offset < len; ++offset) {
      buf[offset] = ::mix(buf[offset], soundBuf[offset]);
    }

    snd.offset += len;
  }

  for(int i = sounds.size() - 1; i>=0; --i) {
    auto const& a = sounds[i];
    if(a.offset == a.sound->getSampleCount())
      sounds.erase(sounds.begin() + i);
  }
  /*
  auto it = std::remove_if(sounds.begin(), sounds.end(), 
      [](auto const& a) {
        bool r = a.offset == a.sound->getSampleCount(); 
        return r;
      });

  if(it != sounds.end())
    sounds.erase(it);
  */

  canmix = false;
}

