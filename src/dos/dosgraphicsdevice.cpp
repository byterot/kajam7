#include "graphicsdevice.h"

#include "dpmi.h"   // for interrupts
#include "pc.h"     // for ports
#include <sys/nearptr.h>

#include <cstdint>
#include <stdlib.h> // malloc
#include <string.h> // memcpy

#define VGA_STRIDE 320
#define VGA_HEIGHT 200
#define DOUBLE_BUFFER_HEIGHT 200 //165 // height of double buffer

static uint8_t *double_buffer;
static uint8_t *tile_backbuffer;

static void setmode(uint32_t mode) {
	__dpmi_regs r;
	r.x.ax = mode;
	__dpmi_int(0x10, &r);
};

GraphicsDevice::GraphicsDevice() {
	setmode(0x13);	
	double_buffer_enabled = true;
	vgastart = (uint32_t)(vgastart + __djgpp_conventional_base);
	
	// TODO: implement error handling, if malloc fails, make it known
	double_buffer = (uint8_t *)malloc(VGA_STRIDE*DOUBLE_BUFFER_HEIGHT);
	for(uint32_t i = 0; i < VGA_STRIDE*DOUBLE_BUFFER_HEIGHT; ++i)
		double_buffer[i] = 0;
	
	tile_backbuffer = (uint8_t *)malloc(VGA_STRIDE*DOUBLE_BUFFER_HEIGHT);
	for(uint32_t i = 0; i < VGA_STRIDE*DOUBLE_BUFFER_HEIGHT; ++i)
		tile_backbuffer[i] = 0;
};
GraphicsDevice::~GraphicsDevice() {
	//free(double_buffer);
	//free(tile_backbuffer);
	setmode(0x3);
};

void GraphicsDevice::pixel(uint32_t x, uint32_t y, uint8_t color) {
	if(y< DOUBLE_BUFFER_HEIGHT && double_buffer_enabled) {
		*(double_buffer + (y * VGA_STRIDE) + x) = color;
		return;
	}
	uint8_t volatile *vgaptr = (uint8_t volatile *)(vgastart);
	vgaptr += (y * VGA_STRIDE) + x;
	*vgaptr = color;
};

void GraphicsDevice::backpixel(uint32_t x, uint32_t y, uint8_t color) {
	//if(y >= DOUBLE_BUFFER_HEIGHT || x >= VGA_STRIDE) return;
	//if(x < 0 || y < 0) return;
	tile_backbuffer[y * VGA_STRIDE + x] = color;
};

void GraphicsDevice::undraw(uint32_t x, uint32_t y) {
	//if(y >= DOUBLE_BUFFER_HEIGHT || x >= VGA_STRIDE) return;
	//if(x < 0 || y < 0) return;
	double_buffer[y * VGA_STRIDE + x] = tile_backbuffer[y * VGA_STRIDE + x];
};

void GraphicsDevice::undrawbuffer() {
	for(uint32_t y = 0; y < DOUBLE_BUFFER_HEIGHT; ++y)
		for(uint32_t x = 0; x < VGA_STRIDE; ++x)
			undraw(x,y);
};

void GraphicsDevice::palette(uint8_t color, 
														 uint8_t r, uint8_t g, uint8_t b) {
	// VGA DAC address register: 0x3C8
	// VGA DAC data register: 0x3C9
	outportb(0x3C8, color);
	outportb(0x3C9, r);
	outportb(0x3C9, g);
	outportb(0x3C9, b);
};

void GraphicsDevice::getcolor(uint8_t color, uint8_t &r, uint8_t &g, uint8_t &b) {
	// VGA DAC address register: 0x3C7 (for reading)
	// VGA DAC data register: 0x3C9
	outportb(0x3C7, color);
	r = inportb(0x3C9);
	g = inportb(0x3C9);
	b = inportb(0x3C9);
};

void GraphicsDevice::draw(void) {
	memcpy((uint8_t *)vgastart, double_buffer, VGA_STRIDE*DOUBLE_BUFFER_HEIGHT);
	// VSync
	do{}
	while(!(inportb(0x3DA) & 8));
};

void GraphicsDevice::clear(uint8_t color) {
	for(uint16_t x = 0; x < VGA_STRIDE; ++x)
		for(uint16_t y = 0; y < VGA_HEIGHT; ++y)
			pixel(x,y, color);
	/*uint32_t volatile *vga = (uint32_t *)(vgastart);
	for(uint32_t i = 0; i < 16000; ++i)
			vga[i] = color << 24 | color < 16 | color < 8 | color;*/
};
