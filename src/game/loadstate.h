#pragma once

#include "gamestate.h"
#include "game.h"

class LoadState : public GameState {
	public:
		LoadState(Game *g);
		~LoadState();
		void update();
		void draw();
};
