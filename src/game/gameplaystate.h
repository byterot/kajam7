#pragma once

#include "gamestate.h"

#include <cstdint>
#include <memory>

#include "renderer.h"
#include "map.h"
#include "fpa.h"

class GameplayState : public GameState {
	private: 
		std::unique_ptr<Map> map;
		std::unique_ptr<Renderer> renderer;	
		
		void checkcollision(const struct Sector *sect, float px, float py, float *dx, float *dy);
		void checkraycollision(int *sectnum, float px, float py, float *dx, float *dy);
		void checkmonstercollision(struct MapSprite *monst, float *dx, float *dy);
	public:
		GameplayState(Game *g);
		~GameplayState();
		void update();
		void draw();
};
