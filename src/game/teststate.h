#pragma once

#include "gamestate.h"

#include <cstdint>
#include <memory>

class TestState : public GameState {
	public:
		TestState(Game *g);
		~TestState();
		void update();
		void draw();
};
