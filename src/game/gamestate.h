#pragma once

#include <cstdint>
#include <memory>

#include "graphicsdevice.h"
#include "contentmanager.h"
#include "fpa.h"
#include "game.h"

class Game;

class GameState {
	protected:
		Game *game;
	public:
		GameState(Game *g) : game(g) {};
		virtual ~GameState() {};

		virtual void update() = 0;
		virtual void draw() = 0;
};

