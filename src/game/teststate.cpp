#include "teststate.h"

#include <cstdint>
#include <memory>
#include <string>

#include "dos.h"
#include "pcximage.h"
#include "sprite.h"

TestState::TestState(Game *g) : GameState(g) {
	return;
	game->graphics.clear(0);
	game->graphics.draw();
	auto image = game->content.get_image(std::string("wall0.pcx"));
	Sprite spr;
	spr.set_image(image);
	spr.draw(game->graphics);
	delay(1000);
}

TestState::~TestState(){
	
}

void TestState::update() {

}
void TestState::draw() {
	
}
