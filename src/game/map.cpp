#include "map.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <cstdint>
#include <string>
#include <dos.h>
#include <stdio.h>

Map::Map(FILE *fp) {
	fgetc(fp); fgetc(fp); // we don't need the length of the file.
	struct Sector sector;
	struct Wall wall;
	struct MapSprite sprite;

	int16_t numsectors, numwalls, numsprites;
	int32_t mapversion, startx, starty, startz;
	int16_t start_angle, start_sector;

	fread(&mapversion, sizeof(int32_t), 1, fp);
	fread(&startx, sizeof(int32_t), 1, fp);
	fread(&starty, sizeof(int32_t), 1, fp);
	fread(&startz, sizeof(int32_t), 1, fp); // Z values are shifted up by 4
	
	fread(&start_angle, sizeof(int16_t), 1, fp);
	fread(&start_sector, sizeof(int16_t), 1, fp);

	player.x = startx;
	player.y = starty;
	player.z = startz;
	player.angle = (start_angle/2048.f) * (M_PI /2.f);
	player.sectnum = start_sector;
	player.yaw = 0;
	
	fread(&numsectors, sizeof(int16_t), 1, fp);

	for(int16_t i = 0; i < numsectors; ++i) {
		fread(&sector, sizeof(struct Sector), 1, fp);
		//int32_t tmp = sector.ceilingz;
		sector.ceilingz = -sector.ceilingz;
		sector.floorz = -sector.floorz;
		//sector.ceilingz = sector.floorz;
		//sector.floorz = tmp;
		//sector.floorz = 0;
		//sector.ceilingz = 128;
		sector.floorz /= 96;
		sector.ceilingz /= 96;
		sectors.push_back(sector);
	}
	player.z = sectors.at(player.sectnum).floorz + player.height;
	
	fread(&numwalls, sizeof(int16_t), 1, fp);

	for(int16_t i = 0; i < numwalls; ++i) {
		fread(&wall, sizeof(struct Wall), 1, fp);
		wall.x /= 16;
		wall.y /= 16;
		walls.push_back(wall);
	}

	fread(&numsprites, sizeof(int16_t), 1, fp);
	for(int16_t i = 0; i < numsprites; ++i) {
		fread(&sprite, sizeof(struct MapSprite), 1, fp);
		sprite.x /= 16;
		sprite.y /= 16;
		sprite.z = sectors[sprite.sectnum].floorz + 32;
		sprite.extra = 100;
		sprite.picnum -= 1;
		
		sprites.push_back(sprite);
	}

/*
	printf("Start xyz: %d, %d, %d. angle: %d\n", startx, starty, startz, start_angle);
	printf("Size of Sector: %d, Wall: %d, Sprite: %d\n", sizeof(struct Sector), sizeof(struct Wall), sizeof(MapSprite));
	printf("Read %d/%d sectors\n", sectors.size(), numsectors);
	printf("Read %d/%d walls\n", walls.size(), numwalls);
	printf("Read %d/%d sprites\n", sprites.size(), numsprites);

	for(int i = 0; i < walls.size(); ++i)
		printf("Wall %d x,y = %d,%d texture = %d\n", i, walls.at(i).x, walls.at(i).y, walls.at(i).picnum);
*/
	fclose(fp);	
};
Map::~Map() {}
