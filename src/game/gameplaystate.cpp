#include "gameplaystate.h"

#include <time.h>
#include <stdlib.h>

#include <cstdint>
#include <string>
#include <memory>

#include "dos.h"
#include "font.h"
#include "sprite.h"
#include "clip.h"
#include "math.h"
#include "utils.h"
#include "pcximage.h"

static std::string text("this is a string");
static std::unique_ptr<Font> font;
static std::shared_ptr<PcxImage> gunimage;

static int32_t gunframe = 0;
static int32_t spawntimer = 0,
								numspawnpoints = 0;

static void drawgun(GraphicsDevice &g, int32_t fx, int32_t fy) {
	const int gun_width = 28;
	const int gun_height = 48;

	uint32_t	sx = 160 - (gun_width / 2),
 						sy = 195 - gun_height,
						ex = 160 + (gun_width / 2),
						ey = 195;

	uint32_t 	tsx = fx * gun_width,
						tsy = fy * gun_height,
						tex = tsx + gun_width,
						tey = tsy + gun_height,
						tx = tsx,
						ty = tsy;

	for(uint32_t x = sx; x < ex; ++x) {
		for(uint32_t y = sy; y < ey; ++y) {
			uint8_t col = gunimage->data[ty * gunimage->width + tx];
			if(col == 0) {
				++ty;
				continue;
			}
			g.pixel(x,y, col);
			++ty;
		}
		ty = tsy;
		++tx;
	}
}

GameplayState::GameplayState(Game *g) : GameState(g) {
	srand(time(NULL));
	game->graphics.clear(0);
	game->graphics.draw();
	std::string fontimage("font2.pcx");

	game->graphics.clear(4);
	
	font = std::make_unique<Font>(game->content.get_image(fontimage));

	map = std::make_unique<Map>(game->resources.find(std::string("THREE.MAP")));

	for(auto& s : map->sprites) {
		if(s.picnum < 0) numspawnpoints++;
	}

	renderer = std::make_unique<Renderer>(game->content);

	game->sound.load_sound(std::string("test"), std::string("test3.wav"));	
	game->sound.load_sound(std::string("test2"), std::string("test2.wav"));	
	game->sound.load_opl3(game->resources.find(std::string("first.rad")));
	game->sound.play_opl3();

	gunimage = game->content.get_image(std::string("guns.pcx"));
}

GameplayState::~GameplayState() {

}

void GameplayState::checkcollision(const struct Sector *sect, float px, float py, float *dx, float *dy) {
	const struct Wall *wall = &map->walls.at(sect->wallstart);
	const struct Wall *nextwall; 

	for(int16_t i = 0; i < sect->wallcount; ++i) {
		nextwall = &map->walls.at(wall->point2);
		float wx = (wall->x),
					wy = (wall->y),
					nwx = (nextwall->x),
					nwy = (nextwall->y);

		if(wall->nextsector >= 0) {
			if(pointside(px + *dx,py + *dy, wx,wy, nwx,nwy) <= 0 && // we intersect with the wall line
					intersectbox(px,py, px + *dx,py + *dy, wx,wy, nwx,nwy)) { // we collide with the wall

				//TODO: check if player can get in height wise
				const struct Sector *const nsect = &map->sectors.at(wall->nextsector);
				if(nsect->ceilingz > map->player.z + map->player.head_height && 
					nsect->floorz < map->player.z - map->player.height + map->player.knee_height) {

					// put player in sector
					map->player.z = map->sectors.at(wall->nextsector).floorz + map->player.height;
					map->player.sectnum = wall->nextsector; 

					wall = nextwall;	
					continue;
				}
			}
		}
		
		if(!(pointside(px + *dx,py + *dy, wx,wy, nwx,nwy) <= 0 && 
					intersectbox(px,py, px + *dx,py + *dy, wx,wy, nwx,nwy))) { // we do not collide with this wall, continue 
			wall = nextwall;
			continue; // we do not collide with this wall. 
		}
		if(pointside(px,py + *dy, wx,wy, nwx,nwy) <= 0) {
			if(pointside(px + *dx,py, wx,wy, nwx,nwy) <= 0) {
				*dx = 0;
				*dy = 0;
			} else {
				*dy = 0;
			}
		}else {
			*dx = 0;
		}
		wall = nextwall;
	}	
}

void GameplayState::checkraycollision(int *sectnum, float px, float py, float *dx, float *dy) {
	const struct Sector *sect = &map->sectors.at(*sectnum);
	const struct Wall *wall = &map->walls.at(sect->wallstart);
	const struct Wall *nextwall; 

	for(int16_t i = 0; i < sect->wallcount; ++i) {
		nextwall = &map->walls.at(wall->point2);
		float wx = (wall->x),
					wy = (wall->y),
					nwx = (nextwall->x),
					nwy = (nextwall->y);

		if(wall->nextsector >= 0) {
			if(pointside(px + *dx,py + *dy, wx,wy, nwx,nwy) <= 0 && // we intersect with the wall line
					intersectbox(px,py, px + *dx,py + *dy, wx,wy, nwx,nwy)) { // we collide with the wall

				//TODO: check if player can get in height wise
				const struct Sector *const nsect = &map->sectors.at(wall->nextsector);
				if(nsect->ceilingz > map->player.z + map->player.head_height && 
					nsect->floorz < map->player.z - map->player.height + map->player.knee_height) {

					*sectnum = wall->nextsector; 

					wall = nextwall;	
					continue;
				}
			}
		}
		
		if(!(pointside(px + *dx,py + *dy, wx,wy, nwx,nwy) <= 0 && 
					intersectbox(px,py, px + *dx,py + *dy, wx,wy, nwx,nwy))) { // we do not collide with this wall, continue 
			wall = nextwall;
			continue; // we do not collide with this wall. 
		}
		if(pointside(px,py + *dy, wx,wy, nwx,nwy) <= 0) {
			if(pointside(px + *dx,py, wx,wy, nwx,nwy) <= 0) {
				*dx = 0;
				*dy = 0;
			} else {
				*dy = 0;
			}
		}else {
			*dx = 0;
		}
		wall = nextwall;
	}	
}

void GameplayState::checkmonstercollision(struct MapSprite *monst, float *dx, float *dy) {
	const struct Sector *sect = &map->sectors.at(monst->sectnum);
	const struct Wall *wall = &map->walls.at(sect->wallstart);
	const struct Wall *nextwall; 

	float px = monst->x,
				py = monst->y;
	if(monst->z > sect->floorz + 40)
		monst->z-=5;
	if(monst->z < sect->floorz + 40)
		monst->z+=5;

	for(int16_t i = 0; i < sect->wallcount; ++i) {
		nextwall = &map->walls.at(wall->point2);
		float wx = (wall->x),
					wy = (wall->y),
					nwx = (nextwall->x),
					nwy = (nextwall->y);

		if(wall->nextsector >= 0) {
			if(pointside(px + *dx,py + *dy, wx,wy, nwx,nwy) <= 0 && // we intersect with the wall line
					intersectbox(px,py, px + *dx,py + *dy, wx,wy, nwx,nwy)) { // we collide with the wall

				//TODO: check if player can get in height wise
				const struct Sector *const nsect = &map->sectors.at(wall->nextsector);
				if(nsect->ceilingz > monst->z + 40 && 
					nsect->floorz < monst->z) {

					// put player in sector
					//monst->z = map->sectors.at(wall->nextsector).floorz + 40;
					monst->sectnum = wall->nextsector; 

					wall = nextwall;	
					continue;
				}
			}
		}
		
		if(!(pointside(px + *dx,py + *dy, wx,wy, nwx,nwy) <= 0 && 
					intersectbox(px,py, px + *dx,py + *dy, wx,wy, nwx,nwy))) { // we do not collide with this wall, continue 
			wall = nextwall;
			continue; // we do not collide with this wall. 
		}
		if(pointside(px,py + *dy, wx,wy, nwx,nwy) <= 0) {
			if(pointside(px + *dx,py, wx,wy, nwx,nwy) <= 0) {
				*dx = 0;
				*dy = 0;
			} else {
				*dy = 0;
			}
		}else {
			*dx = 0;
		}
		wall = nextwall;
	}	
}

/*
static float time = .0f,
						 delta = 1.0f / 35.f,
						 acc = .0f;
*/

void GameplayState::update() {
	const float turnrad = 1.57079633f; // 90 degrees in radians

	float anglerad = map->player.angle;
	float playercos = cos(anglerad);
	float playersin = sin(anglerad);

	float playerspeed = 4;
	float turnspeed = 0.05f;
	float	deltax = 0,
				deltay = 0;
	
	int16_t mousedelta = game->input.mouse_x - 160;

	bool moving = false;
	if(game->input.key_down(0x2A)) { // LShift
		playerspeed *= 2;
		turnspeed *= 3;
	}
	if(game->input.key_down(0x11)) { // W
	  deltax += playercos * playerspeed;
		deltay += playersin * playerspeed;
		moving = true;
	}
	if(game->input.key_down(0x1f)) { // S
		deltax -= playercos * playerspeed;
		deltay -= playersin * playerspeed;
		moving = true;
	}

	if(game->input.mouse_enabled)	
		map->player.angle += mousedelta * .01f;
	game->input.set_mouse_pos(160, 100);

		if(game->input.key_down(0x10)) { // A
		map->player.angle -= turnspeed;
  }
  if(game->input.key_down(0x12)) { // A
		map->player.angle += turnspeed;
  }
  if(game->input.key_down(0x1E)) { // A
	  deltax -= cos(anglerad+turnrad) * playerspeed;
		deltay -= sin(anglerad+turnrad) * playerspeed;
		moving = true;
  }
  if(game->input.key_down(0x20)) { // D
	  deltax += cos(anglerad+turnrad) * playerspeed;
		deltay += sin(anglerad+turnrad) * playerspeed;
		moving = true;
  }
	
	if(game->input.key_pressed(0x21)) { // F
	}
	if(game->input.key_pressed(0x26)) { // L
		renderer->lameflats = !renderer->lameflats;
	}

	if(gunframe > 0) ++gunframe;
	gunframe %= 8;
	if(gunframe == 0 && (game->input.mouse_pressed(1) || game->input.key_down(0x21))) {
		game->sound.play_sound(std::string("test"));
		++gunframe;	

		int sectnum = map->player.sectnum;
		int tries = 0, hit = 0;
		float px = map->player.x,
					py = map->player.y,
					dx = playercos * 4,
					dy = playersin * 4;
		while(dx != 0 && dy != 0 && tries < 1024) {
			checkraycollision(&sectnum, px, py, &dx,&dy);
			px += dx;
			py += dy;
			tries++;

			for(auto it = map->sprites.begin(); it != map->sprites.end(); ++it) {
				if(it->picnum < 0) continue;
				if(hit = (abs(it->x - px) < 16 && abs(it->y - py) < 16)) {
					it->extra -= 34;
					if(it->extra <= 0) {
						map->sprites.erase(it);
					}
					break;
				}
			}
			if(hit) break;
		}
	}

	if(moving) {
		float px = map->player.x,
					py = map->player.y,
					dx = (deltax),
					dy = (deltay);

		const struct Sector *const sect = &map->sectors.at(map->player.sectnum);
		const struct Wall *wall = &map->walls.at(sect->wallstart);

		checkcollision(sect, px,py, &dx,&dy);
		if(dx > 0 || dy > 0)
		{
			wall = &map->walls.at(sect->wallstart);
			for(int16_t i = 0; i < sect->wallcount; ++i) {
				if(wall->nextsector >= 0) checkcollision(&map->sectors.at(wall->nextsector), px,py, &dx,&dy);
				wall = &map->walls.at(wall->point2);
			}
		}
		map->player.x = (int)(px + dx);
		map->player.y = (int)(py + dy);;
	}
	for(auto& s : map->sprites) {
		if(s.picnum < 0) continue;
		float dx = clamp((float)(map->player.x - s.x), -4.0f,4.0f),
					dy = clamp((float)(map->player.y - s.y), -4.0f,4.0f);
		checkmonstercollision(&s, &dx, &dy);
		s.x = (s.x + dx);
		s.y = (s.y + dy);
		if(abs(s.x - map->player.x) < 32 && abs(s.y - map->player.y) < 32) 
			map->player.health-=5;
	}


	if(spawntimer <= 0 && numspawnpoints > 0) {
		for(auto& s : map->sprites) {
			if(s.picnum < 0) {
				if(rand() % 50 < 25) continue;
					MapSprite n;
					n.x = s.x;
					n.y = s.y;
					n.z = s.z;
					n.extra = 100;
					n.picnum = 1;
					n.sectnum = s.sectnum;
					map->sprites.push_back(std::move(n));
					//delay(100);
					game->sound.play_sound(std::string("test2"));
					break;
			}
		}

		spawntimer = 30 + (rand() % 10);
	}
	spawntimer--;
	if(map->player.health <= 0)
		game->exit = true;
}

void GameplayState::draw() {
	//font->draw_string(game->graphics, text, 0,0);
	if(game->input.key_down(0x39)) { // spacebar
	game->graphics.clear(0);
		game->graphics.double_buffer_enabled = false;
		game->graphics.clear(0);
		renderer->draw(game->graphics, *map, true);
		game->graphics.double_buffer_enabled = true;
	}else 
		renderer->draw(game->graphics, *map, false);

	drawgun(game->graphics, gunframe,0);
	
	int barsize = (int)(320.0f * (map->player.health / 1000.0f));
	for(int x = 0; x < 320; ++x) {
		game->graphics.pixel(x,195,0);
		uint8_t col = 0;
		if(x > 160 - barsize / 2 && x < 160 + barsize / 2)
			col = 28;
		for(int y = 196; y < 200; ++y)
			game->graphics.pixel(x,y, col);
	}
	
}
