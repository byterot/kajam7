#include "loadstate.h"

#include <cstdint>
#include <string>
#include <memory>

#include "dos.h"
#include "gameplaystate.h"
#include "font.h"
#include "pcximage.h"

static std::string loadtext("Loading...");
static std::shared_ptr<Font> font;

LoadState::LoadState(Game *g) : GameState(g) {
	font = std::make_shared<Font>(game->content.get_image(std::string("font.pcx")));
}

LoadState::~LoadState() {

}

void LoadState::update() {
	/*draw();
	delay(1000);
	loadtext = std::string("Still loading...");
	draw();
	delay(1000);
*/
	game->set_state(std::make_shared<GameplayState>(game));
}

void LoadState::draw() {
	game->graphics.clear(0);
	font->draw_string(game->graphics, loadtext, 0, 0);
	game->graphics.draw();
}
