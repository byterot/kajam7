#include "renderer.h"

// ----------------------------------------------------------
// | Big thanks to Bisqwit for the following rendering code |
// | Source: https://www.youtube.com/watch?v=HQYsFshbkYw    |
// | Adapted to suit my needs :)														|
// ----------------------------------------------------------

#include <string>
#include "fpa.h"
#include "math.h"
#include "dos.h"
#include "utils.h"
#include "contentmanager.h"
#include "pcximage.h"

#define RENDER_WIDTH 320
#define RENDER_HEIGHT 195
#define TEXTURE_SIZE 64 // in pixels
#define PALETTE_SIZE 32
#define SHADE_COUNT 256 / PALETTE_SIZE - 1

static const float fov = 0.73f;
static const float hfov = 1.f * fov * (float)RENDER_HEIGHT / (float)RENDER_WIDTH, vfov = .2f; // horizontal and vertical field of view
static const int32_t render_width_mid = RENDER_WIDTH >> 1;
static float fovdiv_vlookup[RENDER_HEIGHT],
						 fovdiv_hlookup[RENDER_WIDTH];

// Zbuffer, stores columns. not rows.
static int32_t	*zbuffer;

static inline void vline(GraphicsDevice &g, int32_t x, int32_t y0, int32_t y1, uint8_t color) {
	for(int32_t y = y0; y <= y1; ++y)
		g.pixel(x,y, color);
}

inline void Renderer::wallline(GraphicsDevice &g, int32_t x, int32_t y1, int32_t y2, struct Scaler ty, uint32_t txtx, int32_t z, int32_t *zb, int16_t texture) const {
	y1 = clamp((int)y1, 0, RENDER_HEIGHT - 1);
	y2 = clamp((int)y2, 0, RENDER_HEIGHT - 1);
	
	uint8_t shift = min(z / 640, (int32_t)SHADE_COUNT) * PALETTE_SIZE;
	zb += x * RENDER_HEIGHT;
	zb += y1;
	for(int32_t y = y1; y <= y2; ++y) {
		uint32_t txty = Scaler_Next(&ty);
		if(*zb < z) {
			++zb;
			continue;
		}
		*zb = z;
		++zb;
		g.pixel(x,y, textures[texture]->data[((txty % TEXTURE_SIZE) * TEXTURE_SIZE) + txtx % TEXTURE_SIZE] + shift);
	}
}

inline void Renderer::spriteline(GraphicsDevice &g, int32_t x, int32_t y1, int32_t y2, struct Scaler ty, uint32_t txtx, int32_t z, int32_t *zb, int16_t texture) const {
	y1 = clamp((int)y1, 0, RENDER_HEIGHT - 1);
	y2 = clamp((int)y2, 0, RENDER_HEIGHT - 1);
	
	uint8_t shift = min(z / 640, (int32_t)SHADE_COUNT) * PALETTE_SIZE;
	zb += x * RENDER_HEIGHT;
	zb += y1;
	for(int32_t y = y1; y <= y2; ++y) {
		uint32_t txty = Scaler_Next(&ty);
		uint8_t col = sprites[texture]->data[((txty % TEXTURE_SIZE) * TEXTURE_SIZE) + txtx % TEXTURE_SIZE];
		if(*zb < z || col == 0) {
			++zb;
			continue;
		}
		*zb = z;
		++zb;
		g.pixel(x,y, col + shift);
	}
}

Renderer::Renderer(ContentManager &content) {
	for(int32_t i = 0; i < RENDER_HEIGHT; ++i) {
		fovdiv_vlookup[i] = RENDER_HEIGHT * vfov / ((RENDER_HEIGHT/2 - i));
	}
	for(int32_t i = 0; i < RENDER_WIDTH; ++i) {
		fovdiv_hlookup[i] = (render_width_mid - i) / (RENDER_WIDTH * hfov);
	}

	for(auto it = content.resource_names.begin(); it != content.resource_names.end(); ++it) {
		if(str_startswith(std::string("wall"), *it)) 
			textures.push_back(content.get_image(*it));
		if(str_startswith(std::string("sprite"), *it)) 
			sprites.push_back(content.get_image(*it));
	}

	zbuffer = (int32_t *)malloc(sizeof(int32_t) * RENDER_WIDTH * RENDER_HEIGHT);
}

Renderer::~Renderer() {
	free(zbuffer);	
}

void Renderer::draw(GraphicsDevice &g, Map &m, bool animate) {
		// build engine defines angles as 2048 units in one revolution
		float playeranglerad = m.player.angle;
		float playercos = cos(playeranglerad),
					playersin = sin(playeranglerad);
		float	playerx = m.player.x, 
					playery = m.player.y,
					playerz = m.player.z,
					playeryaw = m.player.yaw;

    enum { MaxQueue = 64 };  // maximum number of pending portal renders
    struct item { int32_t sectorno,sx1,sx2; } queue[MaxQueue], *head=queue, *tail=queue;
    int32_t ytop[RENDER_WIDTH]={0}, 
						ybottom[RENDER_WIDTH], 
						renderedsectors[m.sectors.size()],
						sectorrenderorder[MaxQueue],
						sectindex;
		
    for(unsigned x=0; x<RENDER_WIDTH; ++x) ybottom[x] = RENDER_HEIGHT-1;
    for(unsigned n=0; n<m.sectors.size(); ++n) renderedsectors[n] = 0;
		for(unsigned i=0; i<MaxQueue; ++i) sectorrenderorder[i] = 0;
		for(unsigned i=0; i<RENDER_WIDTH*RENDER_HEIGHT; ++i) zbuffer[i] = INT32_MAX;

		sectindex = 0;
		
    /* Begin whole-screen rendering from where the player is. */
    *head = (struct item) { m.player.sectnum, 0, RENDER_WIDTH-1 };
    if(++head == queue+MaxQueue) head = queue;

    do {
			/* Pick a sector & slice from the queue to draw */
			const struct item now = *tail;
			if(++tail == queue+MaxQueue) tail = queue;

			if(renderedsectors[now.sectorno] & 0x21) continue; // Odd = still rendering, 0x20 = give up
			++renderedsectors[now.sectorno];
			const struct Sector* const sect = &m.sectors.at(now.sectorno);
			struct Wall *wall = &m.walls.at(sect->wallstart);
			struct Wall *nextwall;
			int16_t wallno = sect->wallstart;
			/* Render each wall of this sector that is facing towards player. */
			for(unsigned s = 0; s < sect->wallcount; ++s)
			{
				nextwall = &m.walls.at(wall->point2);
				/* Acquire the x,y coordinates of the two endpoints (vertices) of this edge of the sector */
				float vx1 = wall->x - playerx, vy1 = wall->y - playery;
				float vx2 = nextwall->x - playerx, vy2 = nextwall->y - playery;
				/* Rotate them around the player's view */
				float pcos = playercos, psin = playersin;
				float tx1 = vx1 * psin - vy1 * pcos,  tz1 = vx1 * pcos + vy1 * psin;
				float tx2 = vx2 * psin - vy2 * pcos,  tz2 = vx2 * pcos + vy2 * psin;
			
				float walllength = sqrt(((wall->x - nextwall->x) * (wall->x - nextwall->x)) + ((wall->y - nextwall->y) * (wall->y - nextwall->y)));
				float walllengthmod = 64;
				int32_t u0 = 0, u1 = TEXTURE_SIZE * walllength / walllengthmod;

				/* Is the wall at least partially in front of the player? */
				if(tz1 <= 0 && tz2 <= 0) {
					wallno = wall->point2;
					wall = &m.walls.at(wall->point2); // set the next wall
					continue;
				}
				/* If it's partially behind the player, clip it against player's view frustrum */
				if(tz1 <= 0 || tz2 <= 0)
				{
					float nearz = 1e-4f, farz = 5, nearside = 1e-5f, farside = 20.f;
					// Find an intersection between the wall and the approximate edges of player's view
					Intersect i1 = intersect(tx1,tz1,tx2,tz2, -nearside,nearz, -farside,farz);
					Intersect i2 = intersect(tx1,tz1,tx2,tz2,  nearside,nearz,  farside,farz);
					Intersect<float> org1 = {tx1, tz1}, org2 = {tx2, tz2};

					if(tz1 < nearz) { if(i1.y > 0) { tx1 = i1.x; tz1 = i1.y; } else { tx1 = i2.x; tz1 = i2.y; } }
					if(tz2 < nearz) { if(i1.y > 0) { tx2 = i1.x; tz2 = i1.y; } else { tx2 = i2.x; tz2 = i2.y; } }
				
					float wl = TEXTURE_SIZE * walllength / walllengthmod;
					if(abs(tx2-tx1) > abs(tz2-tz1))
						u0 = (tx1-org1.x) * wl / (org2.x-org1.x), u1 = (tx2-org1.x) * wl / (org2.x-org1.x);
					else
						u0 = (tz1-org1.y) * wl / (org2.y-org1.y), u1 = (tz2-org1.y) * wl / (org2.y-org1.y);

				}
				/* Do perspective transformation */
				float xscale1 = (RENDER_WIDTH*hfov) / tz1, yscale1 = (RENDER_HEIGHT*vfov) / tz1;
				int32_t x1 = render_width_mid - (int)(tx1 * xscale1);

				float xscale2 = (RENDER_WIDTH*hfov) / tz2, yscale2 = (RENDER_HEIGHT*vfov) / tz2; 
				int32_t x2 = render_width_mid - (int)(tx2 * xscale2);

				if(x1 >= x2 || x2 < now.sx1 || x1 > now.sx2) { // Only render if it's visible
					wallno = wall->point2;
					wall = &m.walls.at(wall->point2); // set the next wall
					continue;
				}
				/* Acquire the floor and ceiling heights, relative to where the player's view is */
				float yceil  = sect->ceilingz  - playerz;
				float yfloor = sect->floorz - playerz;
				/* Check the edge type. neighbor=-1 means wall, other=boundary between two sectors. */
				int32_t neighbor = wall->nextsector;
				float nyceil=0, nyfloor=0;
				if(neighbor >= 0) // Is another sector showing through this portal?
				{
					nyceil  = m.sectors.at(neighbor).ceilingz  - playerz;
					nyfloor = m.sectors.at(neighbor).floorz - playerz;
				}
				/* Project our ceiling & floor heights into screen coordinates (Y coordinate) */
#define Yaw(y,z) (y + z*playeryaw)
				int32_t y1a  = RENDER_HEIGHT/2 - (int)(Yaw(yceil, tz1) * yscale1),  
						y1b = RENDER_HEIGHT/2 - (int)(Yaw(yfloor, tz1) * yscale1);
				int32_t y2a  = RENDER_HEIGHT/2 - (int)(Yaw(yceil, tz2) * yscale2),  
						y2b = RENDER_HEIGHT/2 - (int)(Yaw(yfloor, tz2) * yscale2);
				/* The same for the neighboring sector */
				int32_t ny1a = RENDER_HEIGHT/2 - (int)(Yaw(nyceil, tz1) * yscale1), 
						ny1b = RENDER_HEIGHT/2 - (int)(Yaw(nyfloor, tz1) * yscale1);
				int32_t ny2a = RENDER_HEIGHT/2 - (int)(Yaw(nyceil, tz2) * yscale2), 
						ny2b = RENDER_HEIGHT/2 - (int)(Yaw(nyfloor, tz2) * yscale2);

				/* Render the wall. */
				int32_t beginx = max(x1, now.sx1), endx = min(x2, now.sx2);
				
				struct Scaler ya_int = Scaler_Init(x1,beginx,x2, y1a,y2a);
				struct Scaler yb_int = Scaler_Init(x1,beginx,x2, y1b,y2b);
				struct Scaler nya_int = Scaler_Init(x1,beginx,x2, ny1a,ny2a);
				struct Scaler nyb_int = Scaler_Init(x1,beginx,x2, ny1b,ny2b);

				for(int32_t x = beginx; x <= endx; ++x)
				{
					/* Calculate the Z coordinate for this point. (Only used for lighting.) */
					int32_t z = ((x - x1) * (tz2-tz1) / (x2-x1) + tz1) * 8;
					
					int32_t txtx = (u0*((x2-x)*tz2) + u1*((x-x1)*tz1)) / ((x2-x)*tz2 + (x-x1)*tz1);

					/* Acquire the Y coordinates for our floor & ceiling for this X coordinate */
					int32_t ya = Scaler_Next(&ya_int);
					int32_t yb = Scaler_Next(&yb_int);
					/* Clamp the ya & yb */
					int32_t cya = clamp(ya, ytop[x],ybottom[x]);
					int32_t cyb = clamp(yb, ytop[x],ybottom[x]);

					float hei = yceil; // height of either ceiling or floor
					int16_t textureindex = sect->ceilingpicnum;
					// render ceilings and floors
					if(!lameflats) {
						for(int32_t y = ytop[x]; y <= ybottom[x]; y++) {
							// skip the walls
							if(y >= cya && y <= cyb) { 
								y = cyb; 
								hei = yfloor;
								textureindex = sect->floorpicnum;
								continue;
							}
							// Get map X and Z of the current screen coordinate

							float mapz = hei * fovdiv_vlookup[y]; 
							float mapx = mapz * fovdiv_hlookup[x]; 
							float rtx = mapz * pcos + mapx * psin;
							float rtz = mapz * psin - mapx * pcos;
							mapx = rtx + m.player.x;
							mapz = rtz + m.player.y;

							uint32_t txtx = mapx, txtz = mapz;

							uint8_t col = textures[textureindex]->data[(txtz % TEXTURE_SIZE) * TEXTURE_SIZE + txtx % TEXTURE_SIZE];

							// do some lighting
							int32_t flatz = min(abs(y - (RENDER_HEIGHT >> 1)) >> 3, (int32_t)SHADE_COUNT);
							g.pixel(x,y, col + ((SHADE_COUNT - flatz) * PALETTE_SIZE));
						}
					} else {
						for(int32_t y = ytop[x]; y <= ybottom[x]; y++) {
							// skip the walls
							if(y >= cya && y <= cyb) { 
								y = cyb; 
								hei = yfloor;
								textureindex = sect->floorpicnum;
								continue;
							}

							int32_t flatz = min(abs(y - (RENDER_HEIGHT >> 1)) >> 3, (int32_t)SHADE_COUNT);
							g.pixel(x,y, textures[textureindex]->data[0] + ((SHADE_COUNT - flatz) * PALETTE_SIZE));
						}
						/* Render ceiling: everything above this sector's ceiling height. */
						//vline(g, x, ytop[x], cya-1, textures[sect->ceilingpicnum]->data[0] );
						//if(animate) dOpenMPelay(1);
						/* Render floor: everything below this sector's floor height. */
						//vline(g, x, cyb+1, ybottom[x], textures[sect->floorpicnum]->data[0] );
						//if(animate) delay(1);
					}
					/* Is there another sector behind this edge? */
					if(neighbor >= 0)
					{
						/* Same for _their_ floor and ceiling */
						int32_t nya = Scaler_Next(&nya_int);
						int32_t nyb = Scaler_Next(&nyb_int);
						/* Clamp ya2 & yb2 */
						int32_t cnya = clamp(nya, ytop[x],ybottom[x]);
						int32_t cnyb = clamp(nyb, ytop[x],ybottom[x]);

						if(yb == cya || ya == yb) continue;
						//(struct Scaler)Scaler_Init(ya,cya,yb, 1,TEXTURE_SIZE - 1);
						wallline(g, x, cya, cnya-1, (struct Scaler)Scaler_Init(ya,cya,yb, 0,TEXTURE_SIZE - 1), txtx, z, zbuffer, wall->picnum);

						wallline(g, x, cnyb+1, cyb, (struct Scaler)Scaler_Init(ya,cnyb+1,yb, 0,TEXTURE_SIZE - 1), txtx, z, zbuffer, wall->picnum);
						/* If our ceiling is higher than their ceiling, render upper wall */
						unsigned r1 = 0x010101 * (255-z), r2 = 0x040007 * (31-z/8);
//						vline(g, x, cya, cnya-1, 31 - ((int32_t)(z / 640) % 16)); // Between our and their ceiling
						ytop[x] = clamp(max(cya, cnya), ytop[x], (int32_t)(RENDER_HEIGHT-1));   // Shrink the remaining window below these ceilings
						/* If our floor is lower than their floor, render bottom wall */
//						vline(g, x, cnyb+1, cyb, 31 - ((int32_t)(z / 640) % 16)); // Between their and our floor
						ybottom[x] = clamp(min(cyb, cnyb), (int32_t)0, ybottom[x]); // Shrink the remaining window above these floors
					}
					else
					{
						/* There's no neighbor. Render wall from top (cya = ceiling level) to bottom (cyb = floor level). */
						wallline(g, x, cya, cyb, (struct Scaler)Scaler_Init(ya,cya,yb, 0,TEXTURE_SIZE - 1), txtx, z, zbuffer, wall->picnum);
//						vline(g, x, cya, cyb, 31 - ((int32_t)(z / 640) % 16));
					}
					if(animate) delay(1);
				}
				wallno = wall->point2;
				wall = &m.walls.at(wall->point2); // set the next wall
				/* Schedule the neighboring sector for rendering within the window formed by this wall. */
				if(neighbor >= 0 && endx >= beginx && (head+MaxQueue+1-tail)%MaxQueue)
				{
					*head = (struct item) { neighbor, beginx, endx };
					if(++head == queue+MaxQueue) head = queue;
				}
			} // for s in sector's edges
			
			sectorrenderorder[sectindex] = now.sectorno;
			++sectindex;
			++renderedsectors[now.sectorno];
		} while(head != tail); // render any other queued sectors
		
		float planex = cos(m.player.angle + 1.57079633f) * tan(fov),
					planey = sin(m.player.angle + 1.57079633f) * tan(fov);

		#define SPRITE_WIDTH 16
		#define SPRITE_HEIGHT 128
		do {
			for(auto& spr : m.sprites) {
				if(spr.picnum < 0) continue;
				if(spr.sectnum != sectorrenderorder[sectindex]) continue;
				float sx1 = spr.x - playerx,
							sy1 = spr.y - playery;
				float sx2 = sx1,
							sy2 = sy1;

				sx1 -= planex * SPRITE_WIDTH;
				sy1 -= planey * SPRITE_WIDTH;
				sx2 += planex * SPRITE_WIDTH;
				sy2 += planey * SPRITE_WIDTH;

				float tx1 = sx1 * playersin - sy1 * playercos,  tz1 = sx1 * playercos + sy1 * playersin;
				float tx2 = sx2 * playersin - sy2 * playercos,  tz2 = sx2 * playercos + sy2 * playersin;

				//if(tz1 <= 0 || tz2 <= 0) continue;

				int32_t texturewidth = TEXTURE_SIZE, textureheight = TEXTURE_SIZE - 1;
				int32_t u0 = 1, u1 = texturewidth;
				
				/* Do perspective transformation */
				float xscale1 = (RENDER_WIDTH*hfov) / tz1, yscale1 = (RENDER_HEIGHT*vfov) / tz1;
				float xscale2 = (RENDER_WIDTH*hfov) / tz2, yscale2 = (RENDER_HEIGHT*vfov) / tz2; 

				int32_t x1 = render_width_mid - (int)(tx1 * xscale1);
				int32_t x2 = render_width_mid - (int)(tx2 * xscale2);

				if(x1 >= x2 || x2 < 0 || x1 > RENDER_WIDTH) continue;

				float yceil = spr.z - playerz + SPRITE_HEIGHT;
				float yfloor = spr.z - playerz;

				// Project our sprite height:
				int32_t y1a = RENDER_HEIGHT/2 - (int32_t) (Yaw(yceil, tz1)*yscale1), 
								y1b = RENDER_HEIGHT/2 - (int32_t) (Yaw(yfloor, tz1)*yscale1);
				int32_t y2a = RENDER_HEIGHT/2 - (int32_t) (Yaw(yceil, tz2)*yscale2), 
								y2b = RENDER_HEIGHT/2 - (int32_t) (Yaw(yfloor, tz2)*yscale2);

				// Start rendering:
				int32_t beginx = max(x1, (int32_t)0), endx = min(x2, (int32_t)(RENDER_WIDTH - 1));

				Scaler ya_int = Scaler_Init(x1, beginx, x2, y1a, y2a);
				Scaler yb_int = Scaler_Init(x1, beginx, x2, y1b, y2b);

				for (int32_t x = beginx; x <= endx; ++x)
				{
					int32_t z = ((x - x1) * (tz2-tz1) / (x2-x1) + tz1) * 8;

					float txtx = ((u0*((x2 - x) * tz2) + u1 * ((x - x1) * tz1)) / ((x2 - x) * tz2 + (x - x1) * tz1));
					//int32_t txtx = (u0*((x2-x)*tz2) + u1*((x-x1)*tz1)) / ((x2-x)*tz2 + (x-x1)*tz1);

					/* Acquire the Y coordinates for our ceiling & floor for this X coordinate. Clamp them. */
					auto ya = Scaler_Next(&ya_int);
					auto yb = Scaler_Next(&yb_int);
					auto cya = (int32_t) clamp(ya, 0, RENDER_HEIGHT -1); // top
					auto cyb = (int32_t) clamp(yb, 0, RENDER_HEIGHT - 1); // bottom
					spriteline(g, x, cya, cyb, Scaler_Init(ya, cya, yb, 0, TEXTURE_SIZE - 1), txtx, z, zbuffer, spr.picnum - 1);
					//vline(g, x, cya,cyb, 0);
				}


			}
			--sectindex;
		} while(sectindex >= 0);
}
