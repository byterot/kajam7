#pragma once

#include "graphicsdevice.h"
#include "map.h"
#include "contentmanager.h"
#include <vector>
#include <string>
#include <memory>

class Renderer {
	private:
		std::vector<std::shared_ptr<PcxImage>> textures;
		std::vector<std::shared_ptr<PcxImage>> sprites;
		inline void wallline(GraphicsDevice &g, int32_t x, int32_t y1, int32_t y2, struct Scaler ty, uint32_t txtx, int32_t z, int32_t *zb, int16_t texture) const;
		inline void spriteline(GraphicsDevice &g, int32_t x, int32_t y1, int32_t y2, struct Scaler ty, uint32_t txtx, int32_t z, int32_t *zb, int16_t texture) const;
	public:
		bool lameflats = false;
		Renderer(ContentManager &content);
		~Renderer();
		void draw(GraphicsDevice &g, Map &m, bool animate);
};
