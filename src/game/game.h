#pragma once

#include "graphicsdevice.h"
#include "inputdevice.h"
#include "resourcemanager.h"
#include "contentmanager.h"
#include "gamestate.h"
#include "sounddevice.h"

#include <memory>

class GameState;

class Game {
	public:
		ResourceManager resources;
		ContentManager content;
		GraphicsDevice graphics;
		InputDevice input;
		SoundDevice sound;

		bool exit = false;
		//NetworkDevice network; // maybe? probably not

		std::shared_ptr<GameState> state;
		
		Game();
		~Game();

		void set_state(std::shared_ptr<GameState> state);
		bool update();
};
