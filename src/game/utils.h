#pragma once
#include <string>
#include <cstdlib>

#include "fixedpoint.h"
//struct FixedPoint;

static bool str_startswith(std::string needle, std::string haystack) {
	if(needle.size() > haystack.size()) return false;
	for(uint32_t i = 0; i < needle.size(); ++i)
		if(haystack[i] != needle[i]) return false;
	return true;
}
template<typename T>
struct Intersect {
	//double x, y;
	T x,y;
};
struct Scaler { int32_t result, bop, fd, ca, cache; };

#define Scaler_Init(a,b,c,d,f) \
    { d + (b-1 - a) * (f-d) / (c-a), ((f<d) ^ (c<a)) ? -1 : 1, \
      abs(f-d), abs(c-a), (int32_t)((b-1-a) * abs(f-d)) % abs(c-a) }

// Scaler_Next: Return (b++ - a) * (f-d) / (c-a) + d using the initial values passed to Scaler_Init().
static inline int Scaler_Next(struct Scaler* i) {
    for(i->cache += i->fd; i->cache >= i->ca; i->cache -= i->ca) i->result += i->bop;
    return i->result;
}

// Utility functions
//#define min(a,b)             (((a) < (b)) ? (a) : (b)) // min: Choose smaller of two scalars.
//#define max(a,b)             (((a) > (b)) ? (a) : (b)) // max: Choose greater of two scalars.
//#define clamp(a, mi,ma)      min(max(a,mi),ma)         // clamp: Clamp value into set range.
//#define vxs(x0,y0, x1,y1)    ((x0)*(y1) - (x1)*(y0))   // vxs: Vector cross product
// Overlap:  Determine whether the two number ranges overlap.
//#define overlap(a0,a1,b0,b1) (min(a0,a1) <= max(b0,b1) && min(b0,b1) <= max(a0,a1))
// IntersectBox: Determine whether two 2D-boxes intersect.
//#define intersectbox(x0,y0, x1,y1, x2,y2, x3,y3) (overlap(x0,x1,x2,x3) && overlap(y0,y1,y2,y3))
// PointSide: Determine which side of a line the point is on. Return value: <0, =0 or >0.
//#define pointside(px,py, x0,y0, x1,y1) vxs((x1)-(x0), (y1)-(y0), (px)-(x0), (py)-(y0))
// Intersect: Calculate the point of intersection between two lines.
//#define intersect(x1,y1, x2,y2, x3,y3, x4,y4) ((Intersect) { \
    vxs(vxs(x1,y1, x2,y2), (x1)-(x2), vxs(x3,y3, x4,y4), (x3)-(x4)) / vxs((x1)-(x2), (y1)-(y2), (x3)-(x4), (y3)-(y4)), \
    vxs(vxs(x1,y1, x2,y2), (y1)-(y2), vxs(x3,y3, x4,y4), (y3)-(y4)) / vxs((x1)-(x2), (y1)-(y2), (x3)-(x4), (y3)-(y4)) })

template<typename T>
inline constexpr T Tmin(T a, T b) {
	return a < b ? a : b;
}

template<typename T>
inline constexpr T Tmax(T a, T b) {
	return a > b ? a : b;
}

template<typename T>
inline constexpr T clamp(T a, T low, T high) {
	return max(min(a, high), low);
}

template<typename T>
inline constexpr T vxs(T x0, T y0, T x1, T y1) {
	return x0*y1 - x1 * y0;
}
template<typename T>
inline constexpr bool overlap(T a0, T a1, T b0, T b1) {
	return Tmin(a0,a1) <= Tmax(b0,b1) && Tmin(b0,b1) <= Tmax(a0,a1);
}
template<typename T>
inline constexpr bool intersectbox(T x0, T y0, T x1, T y1, T x2, T y2, T x3, T y3) {
	return overlap(x0,x1,x2,x3) && overlap(y0,y1,y2,y3);
}
template<typename T>
inline constexpr T pointside(T px, T py, T x0, T y0, T x1, T y1) {
	return vxs((x1)-(x0), (y1)-(y0), (px)-(x0), (py)-(y0));
}

template<typename T>
inline constexpr Intersect<T> intersect(T x1, T y1, T x2, T y2, T x3, T y3, T x4, T y4) {
	return {
    vxs(vxs(x1,y1, x2,y2), (x1)-(x2), vxs(x3,y3, x4,y4), (x3)-(x4)) / vxs((x1)-(x2), (y1)-(y2), (x3)-(x4), (y3)-(y4)), 
    vxs(vxs(x1,y1, x2,y2), (y1)-(y2), vxs(x3,y3, x4,y4), (y3)-(y4)) / vxs((x1)-(x2), (y1)-(y2), (x3)-(x4), (y3)-(y4))
	};
}

/*template<typename T>
inline constexpr Intersect intersect(T x1, T y1, T x2, T y2, T x3, T y3, T x4, T y4) {

}*/
template<typename T, size_t S>
size_t constexpr array_size(T const (&x)[S]) {
	(void) x;
	return S;
}

#define rep4(x) (((uint32_t)x) | (((uint32_t)x) << 8) | (((uint32_t)x) << 16) | (((uint32_t)x) << 24))


template<typename T>
inline constexpr T lerp(T a, T b, T t) {
	return a + t * (b - a);
}


#define setfast_8(ptr,len,val) \
	asm("mov %0, %%edi\n\t" \
			"mov %1, %%ecx\n\t" \
			"mov %2, %%al\n\t" \
			"rep stosb\n\t" \
			:  \
			: "m" (ptr), "m" (len), "m" (val) \
			: "edi", "ecx", "al", "memory" );

#define setfast_16(ptr,len,val) \
	asm("mov %0, %%edi\n\t" \
			"mov %1, %%ecx\n\t" \
			"movw %2, %%eax\n\t" \
			"rep stosw\n\t" \
			:  \
			: "m" (ptr), "m" (len), "m" (val) \
			: "edi", "ecx", "eax", "memory" );

#define setfast_32(ptr,len,val) \
	asm("mov %0, %%edi\n\t" \
			"mov %1, %%ecx\n\t" \
			"movl %2, %%eax\n\t" \
			"rep stosl\n\t" \
			:  \
			: "m" (ptr), "m" (len), "m" (val) \
			: "edi", "ecx", "eax", "memory" );

