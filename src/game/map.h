#pragma once

#include <cstdint>
#include <vector>
#include <string>
#include <stdio.h>

// ----------------------------------------------------------
// | Source: https://fabiensanglard.net/duke3d/BUILDINF.TXT |
// | Huge thanks go to fabien <3														|
// ----------------------------------------------------------

struct Sector {
	int16_t wallstart, 							// index of first wall in the sector
					wallcount; 							// number of walls in sector
	int32_t ceilingz, floorz; 			// height of sector
	int16_t ceilingstat, floorstat; // unused bitfields

	int16_t ceilingpicnum, 					// ceiling texture index
					ceilingheinum; 					// unused, indicates slope.
	int8_t  ceilingshade; 					// unused, shade difference ceiling
	uint8_t ceilingpal, 						// unused, palette lookup for texture
					ceilingxpanning, 				// texture panning offset x
					ceilingypanning; 				// texture panning offset y
	
	int16_t floorpicnum, 						// floor texture index
					floorheinum; 						// unused, indicates slope.
	int8_t  floorshade; 						// unused, shade difference floor
	uint8_t floorpal, 							// unused, palette lookup for texture
					floorxpanning, 					// texture panning offset x
					floorypanning; 					// texture panning offset y
	
	uint8_t visibility,							// unused, used for distance based lighting
					filler;									// useless byte to align data structure
	int16_t lotag, hitag, extra;		// unused, variables used by duke3d engine
};

struct Wall {
	int32_t x, y;										// x and y coordinate of left hand wall corner
	int16_t point2,									// index of wall denoting right hand wall corner 
					nextwall,								// index of wall on the other side of this wall. (-1 if no sector is behind this wall)
					nextsector,							// Index to sector on the other side of this wall. (-1 if there is no sector)
					cstat;									// unused, bitfield for wall attributes denoting whether wall is translucent/blocking/one-way et cetera.

	int16_t picnum, 								// texture index
					overpicnum;							// unused, texture index for masked (one-way) walls
	int8_t	shade;									// shade offset
	uint8_t	pal,										// unused, palette lookup for texture
					xrepeat,								// unused, used to change the texture size
					yrepeat,								// unused, ditto
					xpanning,								// texture panning offset x
					ypanning;								// texture panning offset y
	int16_t	lotag, hitag, extra;		// unused, variables used by duke3d engine
};

struct MapSprite { // "Sprite" already in use, prefix map used to avoid confusion ;)
	int32_t x, y, z;								// x, y and z coordinates (woah, 3D!)
	int16_t cstat,									// bitfield for sprite attributes: BIT 0: blocking, rest unused
					picnum;									// texture index
	int8_t	shade;									// shade offset
	uint8_t	pal,										// unused, palette lookup for sprite
					clipdist,								// size of clipping square
					filler;									// useless byte to align datastructure
	uint8_t	xrepeat,								// unused, used to change the texture size
					yrepeat;								// unused, ditto
	int8_t	xoffset,								// unused, used for animating sprites
					yoffset;								// unused, ditto
	int16_t sectnum,								// current sector of sprite
					statnum;								// current status of sprite (inactive/monster/bullet et cetera)
	int16_t ang,										// angle of sprite
					owner, xvel, yvel, zvel;// unused, variables used by duke3d engine
	int16_t lotag, hitag, extra;		// unused, variables used by duke3d engine
};

struct Player {
	int32_t x, y, z;								// x, y and z coordinates of player
	float		angle;									// angle of player
	int16_t	sectnum,								// current sector of player
					yaw;
	int32_t head_height=16,
					knee_height=32,
					height=90;
	int32_t health = 1000;
};

class Map {
	public:
		std::vector<Sector> 		sectors;
		std::vector<Wall>				walls;
		std::vector<MapSprite> 	sprites;
		Player player;

		Map(FILE *fp);
		~Map();
};
