#include "game.h"
#include "dos.h"

#include <string>
#include <memory>
#include <stdio.h>
#include "fpa.h"
#include "pos.h"

#include "gameplaystate.h"
#include "loadstate.h"
#include "teststate.h"

Game::Game() {
	resources.init(graphics);
	content.init(&resources);

	auto manifest = resources.resource_manifest();

	//state = std::make_unique<TestState>(this);	
  //return;
	content.get_image(std::string("font2.pcx"));
	state = std::make_unique<LoadState>(this);
}

Game::~Game() {
}

void Game::set_state(std::shared_ptr<GameState> state) {
	this->state = nullptr;
	this->state = state;
};

bool Game::update() {
	sound.update();
	input.update();

	state->update();

	state->draw();
	
	graphics.draw();
	return !input.key_pressed(0x1) && !exit;
}
