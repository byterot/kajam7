#include <cstdio>
#include <sys/nearptr.h>
#include <dos.h>
#include <string>

#include "game.h"

#include "map.h"
#include "sprite.h"

#include "fixedpoint.h"

int main() {
	if(!__djgpp_nearptr_enable()) {
		printf("Could not enable nearptr. :(\n");
		return 1;
	}

	if(0) {
		GraphicsDevice graphics;
		graphics.clear(0);
		graphics.draw();

		ResourceManager resources;
		resources.init(graphics);

		ContentManager content;
		content.init(&resources);

		InputDevice input;
		SoundDevice sound;
		//sound.load_opl3(resources.find(std::string("first.rad")));
		//sound.play_opl3();
		sound.load_sound(std::string("test"), std::string("test.wav"));	
		sound.play_sound(std::string("test"));
		for(int i = 0; i < 200; i++) {
			sound.update();
			delay(10);
		}
		auto image = content.get_image(std::string("guns.pcx"));
		printf("width: %d, height: %d\n", image->width, image->height);
		delay(2500);
		auto c = std::make_shared<Clip>(0,0,320,200);	
		Sprite spr;
		spr.set_clip(c);
		spr.set_image(image);
		spr.draw(graphics);
		graphics.draw();
		delay(500);
	} else {
		Game g;
		while(g.update()) { }
    for(int i = 0; i < 200; i++) {
      g.sound.update();
      delay(10);
    }
	}
	__djgpp_nearptr_disable();
	printf("Thank you for playing!\n");
	return 0;
}
