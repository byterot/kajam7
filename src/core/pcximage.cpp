#include "pcximage.h"

#include <stdio.h>
#include <stdlib.h> // malloc
#include <string>

PcxImage::PcxImage() {
	data = 0;
	width = 0;
	height = 0;
};

PcxImage::~PcxImage() {
	if(data != NULL) { 
		free(data);
		data = NULL;
	}
};

void PcxImage::load(FILE *fp, std::string &n) {
	name = n;
	
	//printf("loading: %s\n", name.c_str());
	// ditch the length, we don't need it
	fgetc(fp);
	fgetc(fp);

	width = fgetc(fp) << 8;
	width = width | fgetc(fp);
	height = fgetc(fp) << 8;
	height = height | fgetc(fp);

	data = (uint8_t *)malloc(width * height);

	uint32_t i = 0;
	uint8_t b, c;
	do {
		b = fgetc(fp);

		if(b & 0xC0) { // check for RLE
			b = b & 0x3F; // remove RLE signature to get run length
			c = fgetc(fp); // get color

			for(uint8_t j = 0; j < b; j++) {
				data[i] = c;
				++i;
				//if(i >= width*height) break;
			}
		} else {
			data[i] = b;
			++i;
		}
	} while(i < width * height);
	
	//printf("image loaded: (%dx%d written: %d / %d)\n", width, height, i, width*height);
};
