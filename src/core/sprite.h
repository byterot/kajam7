#pragma once

#include <cstdint>
#include <memory>

#include "pcximage.h"
#include "graphicsdevice.h"
#include "pos.h"
#include "clip.h"

class Sprite {
	private:
		std::shared_ptr<PcxImage> image;
		//Clip *clip;
		std::shared_ptr<Clip> clip;
		Pos last_pos;
		
		void undraw(GraphicsDevice &g);
	public:
		Pos pos;		
		
		void set_image(std::shared_ptr<PcxImage> i);
		void set_clip(std::shared_ptr<Clip> c);
		void draw(GraphicsDevice &g);
};

