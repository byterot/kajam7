#include "pos.h"
#include "fpa.h"

#include <cstdint>

Pos::Pos() {
	x = fp_create(0);
	y = fp_create(0);
};

void Pos::add(Pos &p) {
	x = fp_add(x, p.x);
	y = fp_add(y, p.y);
};

void Pos::setf(float x, float y) {
	this->x = fp_createf(x);
	this->y = fp_createf(y);
};

void Pos::set(uint32_t x, uint32_t y) {
	this->x = fp_create(x);
	this->y = fp_create(y);
};

void Pos::get(uint32_t &x, uint32_t &y) {
	x = fp_int(this->x);
	y = fp_int(this->y);
};
