#define FP_AUDIT
#include "fixedpoint.h"

static FixedPoint const fp_sin_lut[] = {
# include <fp_sin_lut.h>
};

#define FP_SIN_LUT_SIZE (sizeof(fp_sin_lut)/sizeof(fp_sin_lut[0]))
#define FP_COS_PHASE_SHIFT (FP_SIN_LUT_SIZE / 4)
#define FP_FORCE_POS_0_1(a) (((a) % FP_SIN_LUT_SIZE + FP_SIN_LUT_SIZE ) & (FP_SIN_LUT_SIZE - 1))

// Input is in a multiple of Turn (multiple is defined by size of the lookup table and the FP precision)
FixedPoint sin(FixedPoint a) {
	  // force index into positive [0;1[ range
		  return fp_sin_lut[FP_FORCE_POS_0_1(a.value / FP_TRIG_TABLE_DIVIDER)];
}


// Input is in a multiple of Turn (multiple is defined by size of the lookup table and the FP precision)
FixedPoint cos(FixedPoint a) {
	  // force index into positive [0;1[ range
		  // Just a 90° phase shifted sin
			  return fp_sin_lut[FP_FORCE_POS_0_1((a.value / FP_TRIG_TABLE_DIVIDER) + FP_COS_PHASE_SHIFT)];
}


// I think we'll need those very rarely and clearly not more than a few times per frame
// Better use some processing time instead of a second LUT.
FixedPoint tan(FixedPoint a) {
	  return sin(a) / cos(a);
}


FixedPoint cot(FixedPoint a) {
	  return cos(a) / sin(a);
}
