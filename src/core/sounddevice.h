#pragma once

#include <cstdint>
#include <stdio.h>
#include <string>

class SoundDevice {
	public:
		SoundDevice();
		~SoundDevice();
		// play an opl song
		void load_opl3(FILE *fp);
		void play_opl3();
		void stop_opl3();

		void load_sound(std::string name, std::string file);
		void play_sound(std::string name);
		void update();
};
