#include "resourcemanager.h"

#include <dos.h>
#include <string>

#include "graphicsdevice.h"
#include "pcximage.h"
#include <cstdint>

#define DATA_FILE "KAJAM7.DAT"
// length of palette indicates when to stop loading primary colors and start generating shades
#define PALETTE_LENGTH 32 //255


static uint32_t data_size = 0;

ResourceManager::ResourceManager() {

}

ResourceManager::~ResourceManager() {
}

FILE *ResourceManager::find(std::string name) const {
	FILE *fp = fopen(DATA_FILE, "rb");
	
	uint8_t name_length = 0;
	uint16_t data_length = 0;
	bool match = false;
	
	do {
		match = true;
		name_length =	fgetc(fp);
		for(uint8_t i = 0; i < name_length; ++i) {
			uint8_t r = fgetc(fp);
			if(match) match = r == name[i];
		}
		if(!match) {
			data_length = read_length(fp);
			fseek(fp, data_length, SEEK_CUR);
		}
	}while(!match && ftell(fp) < data_size);
	
	if(!match) {
		fclose(fp);
		fprintf(stderr, "Failed to find asset: %s\n", name.c_str());
		return NULL;
	}
	return fp;
}

uint16_t ResourceManager::read_length(FILE *fp) const {
	uint16_t ret = fgetc(fp) << 8;
	ret = ret | fgetc(fp);
	return ret;
}

void ResourceManager::init(GraphicsDevice &d) const {
	//printf("Preparing data file");
	FILE *fp = fopen(DATA_FILE, "rb");
	
	fseek(fp, 0, SEEK_END);
	data_size = ftell(fp);

	fclose(fp);	
	
	std::string palette = "palette.pcx";
	fp = find(palette);
	fgetc(fp);
	fgetc(fp);

	uint8_t reds[PALETTE_LENGTH], greens[PALETTE_LENGTH], blues[PALETTE_LENGTH];
	
	for(uint8_t c = 0; c < PALETTE_LENGTH; ++c) {
		uint8_t r,g,b;
		r = fgetc(fp) / 4;
		g = fgetc(fp) / 4;
		b = fgetc(fp) / 4;
		d.palette(c, r, g, b);
		reds[c] = r;
		greens[c] = g;
		blues[c] = b;
	}

	uint8_t numshades = (256 - PALETTE_LENGTH) / PALETTE_LENGTH;
	for(uint8_t i = 0; i < numshades; ++i) {
		for(uint8_t c = 0; c < PALETTE_LENGTH; ++c) {
			uint8_t shift = (i+1) * 3;
			uint8_t r = reds[c],
							g = greens[c],
							b = blues[c];

			r -= (r >= shift ? shift : r);
			g -= (g >= shift ? shift : g);
			b -= (b >= shift ? shift : b);

			d.palette((i + 1) * PALETTE_LENGTH + c, r,g,b);
		}
	}

	fclose(fp);
}
void ResourceManager::list(void) const {
	printf("Data file: %s, %d bytes\n", DATA_FILE, data_size);
	
	FILE *fp = fopen(DATA_FILE, "rb");
		
	uint8_t name_length = 0;
	uint16_t data_length = 0;
	do {
		name_length = fgetc(fp);
		printf("   %X:\t", ftell(fp) - 1);

		for(uint8_t i = 0; i < name_length; ++i)
			printf("%c", fgetc(fp));

		data_length = read_length(fp);
		printf("\t-> %d bytes, block start: %X\n", data_length, ftell(fp));
		fseek(fp, data_length, SEEK_CUR);
	} while(ftell(fp) < data_size);	
	fclose(fp);
}

std::vector<std::string> ResourceManager::resource_manifest() const {
	std::vector<std::string> ret;
	FILE *fp = fopen(DATA_FILE, "rb");

	uint8_t name_length = 0;
	uint16_t data_length = 0;
	do {
		name_length = fgetc(fp);
		std::string str;

		for(uint8_t i = 0; i < name_length; ++i)
			str += fgetc(fp);

		ret.push_back(str);
		fseek(fp, read_length(fp), SEEK_CUR);
	} while(ftell(fp) < data_size);
	fclose(fp);

	return ret;
}
