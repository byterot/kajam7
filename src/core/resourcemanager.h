#pragma once

#include <cstdint>
#include <vector>
#include <string>

#include "graphicsdevice.h"

class ResourceManager {
	private:		
		uint16_t read_length(FILE *fp) const;
	public:
		ResourceManager();
		~ResourceManager();

		void init(GraphicsDevice &d) const;

		// lists assets names and sizes
		void list(void) const;

		// attempts to find resource with specified name
		// returns NULL if not found
		FILE *find(std::string name) const;

		std::vector<std::string> resource_manifest() const;
};
