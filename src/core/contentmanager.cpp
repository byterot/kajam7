#include "contentmanager.h"

#include <string>
#include <memory>

#include "pcximage.h"
#include "resourcemanager.h"
#include "graphicsdevice.h"
#include <cstdio>

using namespace std;

ContentManager::ContentManager() {

}

ContentManager::~ContentManager() {
	images.clear();
}

void ContentManager::init(ResourceManager *r) {
	resources = r;
	resource_names = resources->resource_manifest();
};

std::shared_ptr<PcxImage> ContentManager::get_image(string name) {
	for(uint32_t i = 0; i < images.size(); ++i)
		if(images.at(i)->name == name) return images.at(i);
	
	FILE *fp = resources->find(name);
	std::shared_ptr<PcxImage> image = std::make_shared<PcxImage>();
	image->load(fp, name);
	fclose(fp);

	images.push_back(image);
	return image;
};

void ContentManager::unload(std::shared_ptr<PcxImage> image) {
	for(uint32_t i = 0; i < images.size(); ++i)
		if(images.at(i) == image) {
//			delete images.at(i);
			images.erase(images.begin() + i);
			break;
		}
};
