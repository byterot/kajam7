#pragma once

#include <cstdint>
#include "fpa.h"

struct Pos {
	Fixed x, y;
	Pos();
	void add(Pos &p);
	void setf(float x, float y);
	void set(uint32_t x, uint32_t y);
	void get(uint32_t &x, uint32_t &y);
};
