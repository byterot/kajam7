#pragma once

#include <cstdint>
#include <stdio.h>
#include <string>

class PcxImage {
	private:

	public:
		uint8_t *data;
		uint16_t width, height;
		std::string name;

		PcxImage();
		~PcxImage();
		void load(FILE *fp, std::string &n);
};
