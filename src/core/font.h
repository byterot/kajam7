#pragma once

#include <cstdint>
#include <memory>
#include <string>

#include "pcximage.h"
#include "graphicsdevice.h"

class Font {
	private:
	public:
		std::shared_ptr<PcxImage> image;
		Font(std::shared_ptr<PcxImage> i);
		~Font();

		void draw_char(GraphicsDevice &graphics, char ch, int16_t x, int16_t y);
		void draw_string(GraphicsDevice &graphics, std::string &str, int16_t x, int16_t y);
};

